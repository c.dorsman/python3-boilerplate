Python3-boilerplate
===================
A Python3 boiler-plate for developing fast and robust Python3 based applications

What is python3-boilerplate?
----------------------------
``Python3-boilerplate`` is a robust object-oriented boilerplate for Python3 from which you build advanced applications with ease. With features that you want to have as a developer without even to think about it.

Feature overview
----------------
* No usage of third-party libraries 
* Usage of long and short CLI options.
* Advanced parsing configuration file with the option of setting required, non-required and default values. 
* Thorough advanced logging setup
* Easy to customize options.

Requirements
------------
* Python 3.6+

Setup
------------
Just download or git pull the latest version of ``Python3-boilerplate`` and you're done. 
