#!/usr/bin/env python3
"""
Docstring 
"""

__author__ = 'Your name'
__version__ = '0.1'
__license__ = 'MIT'

import sys
import platform
import os
import os.path

import logging
import logging.config
import logging.handlers

from lib.config import *
from lib.parser import *

def main():
    """ Main """ 
    
if __name__ == '__main__':
    try:
        parser = Parser()
        cf = config(parser.config_file)

        if os.path.exists(parser.default_logfile) is False:	
            basedir = os.path.dirname(parser.default_logfile)
            if not os.path.exists(basedir):
               os.makedirs(basedir)
            with open(parser.default_logfile, 'a'):
               os.utime(parser.default_logfile, None)

        __loggername__ = parser.app_name.title()
    	   
        logging.config.fileConfig(fname=parser.default_logconf)
        log = logging.getLogger(__loggername__)
        log.debug('%s-%s running on Python version: %s-%s on %s',
                      __loggername__, 
                      __version__, 
                      platform.python_version(), 
                      platform.machine(), 
                      platform.system())
                      
        log.debug('Configuration settings:\n\n \
Approot: %s |\n \
Logconfig file: %s |\n \
Config file: %s |\n', 
                      parser.app_root, 
                      parser.default_logconf, 
                      parser.config_file)

        main() 
    except:
        print('Something went wrong')
        sys.exit(1)

    

